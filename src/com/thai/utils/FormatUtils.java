package com.thai.utils;

import java.text.DecimalFormat;

public class FormatUtils {
	public static String getMoney(Double money) {
		DecimalFormat formatter = new DecimalFormat("###,###,###");
		String moneyStr = formatter.format(money);
		return moneyStr;
	}
}
