package com.thai.controller.user;

import java.io.FileOutputStream;
import java.io.IOException;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.thai.bean.User;
import com.thai.service.UserService;
import com.thai.service.impl.UserServiceImpl;

public class EditUserController extends SelectorComposer<Component> {

	@Wire
	private Window modalDialog;

	@Wire
	private Image previewImageUser;

	@Wire
	private Textbox codeUser;

	@Wire
	private Textbox fullName;

	@Wire
	private Textbox userName;

	@Wire
	private Textbox passWord;

	
	
	private	UserService userService = new UserServiceImpl();

	private User user = new User();
	
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);

		// Lấy carId được truyền tới bởi trang SearchController qua HashMap
		Integer userId = (Integer) Executions.getCurrent().getArg().get("userId");
		
		Long editUserId = Long.parseLong(String.valueOf(userId));
		// Hiển thị message lên hộp thoại có tính chất minh
//		msg.setValue(userId.toString());
		
		user = userService.findById(editUserId);
		// Gọi phương thức tầng model load dữ liệu từ database căn cứ vào carId có được
		// set vào các trường thuộc tính để hiển thị lên giao diện trang editCar.zul cho
		// phép sửa
		// ??
		
		codeUser.setValue(user.getCode());
		fullName.setValue(user.getFullName());
		userName.setValue(user.getUserName());
		passWord.setValue(user.getPassWord());
		passWord.setDisabled(true);
	}

	@Listen("onClick = #updateUserBtn")
	public void update() {
		// Thêm ảnh vào thư mục img
//		uploadFile();
		// Gọi phương thức của lớp tầng model để insert các trường dữ liệu vào database
		String code = codeUser.getValue().toString();
		String fullName1 = fullName.getValue().toString();
		String userName1 = userName.getValue().toString();
		String password = passWord.getValue().toString();
		String imgUrl = null;
		if (previewImageUser.getContent() != null) {
			imgUrl = previewImageUser.getContent().getName();
		}
		
		user.setCode(code);
		user.setFullName(fullName1);
		user.setUserName(userName1);
		if (imgUrl != null) {
			user.setImg(imgUrl);
		}
		if (userService.save(user)) {
			Messagebox.show("Update new car successfull!");
		} else {
			Messagebox.show("Error");
		}

		// Đóng hộp thoại
		modalDialog.detach();
		// Quay lại trang quản lý ô tô
		Executions.sendRedirect("pageUserAdmin.zul");
	}
	
	public void uploadFile() {
		byte[] byteImg = previewImageUser.getContent().getByteData();
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream("D:\\javatrenlop\\MyZK\\WebContent\\img\\"+previewImageUser.getContent().getName());
			fos.write(byteImg);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				fos.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}
}
