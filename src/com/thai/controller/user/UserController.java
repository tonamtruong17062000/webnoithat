package com.thai.controller.user;

import java.util.HashMap;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.thai.bean.Product;
import com.thai.bean.User;
import com.thai.service.ProductService;
import com.thai.service.UserService;
import com.thai.service.impl.ProductServiceImpl;
import com.thai.service.impl.UserServiceImpl;

public class UserController extends SelectorComposer<Component> {
	@Wire
	private Listbox listUser;

	@Wire
	private Button searchButton;

	@Wire
	private Textbox keywordBox;
	
	@Wire
	private Button clearButton;
	
	private UserService userService = new UserServiceImpl();

	@Override
	public void doAfterCompose(Component component) throws Exception {
		// TODO Auto-generated method stub
		User userSessions = (User) Sessions.getCurrent().getAttribute("userSession");
		if (userSessions == null) {
			Executions.sendRedirect("login.zul");
		} else {
			super.doAfterCompose(component);
			List<User> userList = userService.findBySearch("");
			System.out.println("sakdjak");
			listUser.setModel(new ListModelList<User>(userList));
		}

	}

	@Listen("onClick = #addButton")
	public void showAddModal() {
		Window window = (Window) Executions.createComponents("addUser.zul", null, null);
		window.doModal();
	}

	@Listen("onEdit=#listUser")
	public void showUpdateModalDialog(ForwardEvent evt) {
		// Lấy về đối tượng Edit Button từ ForwardEvent
		Button editBtn = (Button) evt.getOrigin().getTarget();
		// Lấy về id của nút Edit
		String editBtnId = editBtn.getId();
		// Tách ra carId từ id của nút Edit
		Integer userId = Integer.valueOf(editBtnId.substring(4));
		// đưa dữ liệu vào HashMap để truyền vào hộp thoại editCar.zul được mở ra
		final HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("userId", userId);
		Window window = (Window) Executions.createComponents("editUser.zul", null, map);
		window.doModal();
	}

	@Listen("onDelete=#listUser")
	public void doDelete(final ForwardEvent evt) {
		Messagebox.show("Question is pressed. Are you sure?", "Question", Messagebox.YES | Messagebox.NO,
				Messagebox.QUESTION, new EventListener<Event>() {
					@Override
					public void onEvent(final Event confirmEvt) throws Exception {
						// TODO Auto-generated method stub
						if (Messagebox.ON_YES.equals(confirmEvt.getName())) {
							// Tách ra carId từ id của nút Delete
							Button deleteBtn = (Button) evt.getOrigin().getTarget();
							String deleteBtnId = deleteBtn.getId();
							Long userIdDel = Long
									.parseLong(String.valueOf(Integer.valueOf(deleteBtnId.substring(6))));
							// Dòng code minh họa lấy được id
							if (userService.deleteById(userIdDel)) {
								Messagebox.show("Xóa sản phẩm thành công !");
							} else {
								Messagebox.show("Xóa sản phẩm thất bại !");
							}
							Executions.sendRedirect("pageUserAdmin.zul");
						} else { // Nếu ko đồng ý xóa
							return;
						}
					}
				});
	}
	
	
	@Listen("onClick = #searchButton")
	public void search() {
		String keyword = keywordBox.getValue();
		List<User> result = userService.findBySearch(keyword);
		listUser.setModel(new ListModelList<User>(result));
	}
	@Listen("onClick = #clearButton")
	public void reset() {
		keywordBox.setValue("");
	}
}
