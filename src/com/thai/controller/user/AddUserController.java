package com.thai.controller.user;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Blob;
import java.util.List;

import javax.naming.PartialResultException;

import org.zkoss.image.AImage;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Image;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.thai.bean.Product;
import com.thai.bean.User;
import com.thai.service.ProductService;
import com.thai.service.UserService;
import com.thai.service.impl.ProductServiceImpl;
import com.thai.service.impl.UserServiceImpl;
//
//import javafx.animation.ParallelTransition;

public class AddUserController extends SelectorComposer<Component> {

	@Wire
	private Window modalDialog;

	@Wire
	private Image previewImageUser;

	@Wire
	private Textbox codeUser;

	@Wire
	private Textbox fullName;

	@Wire
	private Textbox userName;

	@Wire
	private Textbox passWord;

	private UserService userService = new UserServiceImpl();

	@Listen("onClick = #addUserBtn")
	public void addProduct() {
		// Thêm ảnh vào thư mục img
		if (previewImageUser.getContent() == null) {
			Messagebox.show("Vui lòng chọn ảnh", "", Messagebox.OK, Messagebox.ERROR);
			return;
		}
		// Gọi phương thức của lớp tầng model để insert các trường dữ liệu vào database
		String code = codeUser.getValue().toString();
		String name = fullName.getValue().toString();
		String userName1 = userName.getValue().toString();
		String password = passWord.getValue().toString();
		String imgUrl = previewImageUser.getContent().getName();
		User user = new User(userName1, password, code, "/img/" + imgUrl, name);
		if (userService.save(user)) {
			Messagebox.show("Add new car successfull!");
		} else {
			Messagebox.show("Error");
		}

		// Đóng hộp thoại
		modalDialog.detach();
		// Quay lại trang quản lý ô tô
		Executions.sendRedirect("pageUserAdmin.zul");
	}

	public void uploadFile() {
		byte[] byteImg = previewImageUser.getContent().getByteData();
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(
					"D:\\javatrenlop\\MyZK\\WebContent\\img\\" + previewImageUser.getContent().getName());
			fos.write(byteImg);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				fos.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

}
