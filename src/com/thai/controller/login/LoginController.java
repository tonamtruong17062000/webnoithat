package com.thai.controller.login;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

import com.thai.bean.User;
import com.thai.service.UserService;
import com.thai.service.impl.UserServiceImpl;

public class LoginController extends SelectorComposer<Component>{
	@Wire
	private Textbox userName;
	
	@Wire
	private Textbox password;
	
	@Wire
	private Button submitButton;
	
	
	private UserService userService = new UserServiceImpl(); 
	
	@Override
	public void doAfterCompose(Component component) throws Exception {
		// TODO Auto-generated method stub
		super.doAfterCompose(component);
		userName.setValue("truongduc");
		password.setValue("123456");
		System.out.println("sakdjak");
		
	}
	@Listen("onClick = #submitButton")
	public void login(){
		
		String txt_UserName = userName.getValue().toString();
		String txt_password = password.getValue().toString();
		User user = new User(txt_UserName, txt_password);
		
		if (userService.checkLogin(user)) {
			Sessions.getCurrent().setAttribute("userSession", user);
			Executions.sendRedirect("admin.zul");
		}else {
			Messagebox.show("Tài khoản hoặc mật khẩu không chính xác", "Đăng nhập thất bại", Messagebox.OK,
					Messagebox.ERROR);
		}
	}
}
