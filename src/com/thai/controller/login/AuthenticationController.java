package com.thai.controller.login;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Menu;
import org.zkoss.zul.Menuitem;

import com.thai.bean.User;

public class AuthenticationController extends SelectorComposer<Component>{
	
	@Wire
	private Menuitem menuAdmin;
	
	@Wire
	private Menuitem itemLogout;
	
	@Wire
	private Menu thongTinUser;
	
	@Wire
	private Button btnProduct;

	@Wire
	private Button btnOrder;
	
	@Wire
	private Button btnUser;
	
	@Wire
	private Button btnStatistical;
	@Override
	public void doAfterCompose(Component component) throws Exception {
		// TODO Auto-generated method stub
		super.doAfterCompose(component);
		System.out.println("sakdjak");
//		User userSessions = (User) Sessions.getCurrent().getAttribute("userSession");
//		if (userSessions==null) {
//			thongTinUser.setStyle("display:none");
//		}
	}
	
	@Listen("onClick = #menuAdmin")
	public void redirectAdmin(){
		User userSessions = (User) Sessions.getCurrent().getAttribute("userSession");
		if (userSessions == null) {
			Executions.sendRedirect("login.zul");
		}else {
			Executions.sendRedirect("admin.zul");
		}
	}
	
	@Listen("onClick = #itemLogout")
	public void logout(){
		
		Sessions.getCurrent().removeAttribute("userSession");
		Executions.sendRedirect("home.zul");
	}
	
	
	@Listen("onClick = #btnOrder")
	public void redirectOrderPage() {
		Executions.sendRedirect("pageOrderAdmin.zul");
	}
	
	@Listen("onClick = #btnProduct")
	public void redirectProductPage() {
		Executions.sendRedirect("admin.zul");
	}
	
	@Listen("onClick = #btnUser")
	public void redirectUserPage() {
		Executions.sendRedirect("pageUserAdmin.zul");
	}
	
	@Listen("onClick = #btnStatistical")
	public void redirectStatisticalPage() {
		Executions.sendRedirect("pageStaticAdmin.zul");
	}
}
