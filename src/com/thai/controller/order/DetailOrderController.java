package com.thai.controller.order;

import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;

import com.thai.bean.Cart;
import com.thai.bean.Product;
import com.thai.bean.User;
import com.thai.service.ProductService;
import com.thai.service.impl.ProductServiceImpl;
import com.thai.utils.FormatUtils;
import com.thai.utils.RenderCatpcha;

public class DetailOrderController extends SelectorComposer<Component> {
	@Wire
	private Listbox listProduct;
	
	@Wire
	private Label totalMoney;
	
	private ProductService productService = new ProductServiceImpl();

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		// TODO Auto-generated method stub
		User userSessions = (User) Sessions.getCurrent().getAttribute("userSession");
		Long orderId =Long.parseLong(String.valueOf(Executions.getCurrent().getArg().get("orderId"))) ;
		
		if (userSessions == null) {
			Executions.sendRedirect("login.zul");
		} else {
			List<Product> products = productService.findByOrderId(orderId);
			for (Product product : products) {
				product.setTotalPriceStr(FormatUtils.getMoney(product.getTolTalPrice()));
			}
			totalMoney.setValue("Tổng tiền :"+FormatUtils.getMoney(getTotalMoney(products))+"VND");
			System.out.println("sakdjak");
			listProduct.setModel(new ListModelList<Product>(products));
		}
	}
	
	public static double getTotalMoney(List<Product> products) {
		double totalMoney = 0;
		for (Product itemProduct : products) {
			totalMoney += itemProduct.getTolTalPrice();
		}
		return totalMoney;
	}
}
