package com.thai.controller.order;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.thai.bean.Order;
import com.thai.bean.Product;
import com.thai.service.OrderService;
import com.thai.service.impl.OrderServiceImpl;
import com.thai.utils.RenderCatpcha;

public class HomeOrderController extends SelectorComposer<Component> {

	@Wire
	private Window modalDialog;

	@Wire
	private Textbox fullName;

	@Wire
	private Textbox phone;

	@Wire
	private Textbox address;

	@Wire
	private Label captcha;

	@Wire
	private Textbox txtCaptcha;

	@Wire
	private Button btnPay;

	Double totalMoney = 0.0;

	private OrderService orderService = new OrderServiceImpl();
	private List<Product> listProductInCart = new ArrayList<Product>();

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		totalMoney = (Double) Executions.getCurrent().getArg().get("totalMoney");
		listProductInCart = (List<Product>) Executions.getCurrent().getArg().get("listProductInCart");
		captcha.setValue(RenderCatpcha.renderCaptCha());
	}

	@Listen("onClick = #btnPay")
	public void addProduct() {
		if (!captcha.getValue().equals(txtCaptcha.getValue())) {
			Messagebox.show("Nhập sai mã catpcha", "", Messagebox.OK, Messagebox.ERROR);
			return;
		}
		Order order = new Order("DH" + RenderCatpcha.renderCaptCha(), fullName.getValue(), address.getValue(),
				phone.getValue(), totalMoney);
		order.setProducts(listProductInCart);
		orderService.save(order);
		Messagebox.show("Đặt hàng thành công!");
		// Đóng hộp thoại
		modalDialog.detach();
		// Quay lại trang chủ
		Executions.sendRedirect("home.zul");
		Sessions.getCurrent().removeAttribute("listCart");
		Sessions.getCurrent().removeAttribute("myCart");
	}
}
