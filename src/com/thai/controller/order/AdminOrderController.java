package com.thai.controller.order;

import java.util.HashMap;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.thai.bean.Order;
import com.thai.bean.Product;
import com.thai.bean.User;
import com.thai.service.OrderService;
import com.thai.service.impl.OrderServiceImpl;
import com.thai.utils.FormatUtils;

public class AdminOrderController extends SelectorComposer<Component>{
	@Wire
	private Listbox listOrder;

	@Wire
	private Button searchButton;

	@Wire
	private Textbox keywordBox;
	
	@Wire
	private Button clearButton;
	
	private OrderService orderService = new OrderServiceImpl();
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		User userSessions = (User) Sessions.getCurrent().getAttribute("userSession");
		if (userSessions == null) {
			Executions.sendRedirect("login.zul");
		} else {
			List<Order> orders = orderService.findBySearch("");
			if (orders.size()>0) {
				for (Order order : orders) {
					order.setTotalPriceStr(FormatUtils.getMoney(order.getTotalMoney()));
				}
			}
			listOrder.setModel(new ListModelList<Order>(orders));
		}
	}
	
	@Listen("onClick = #searchButton")
	public void search() {
		String keyword = keywordBox.getValue();
		List<Order> result = orderService.findBySearch(keyword);
		if (result.size()>0) {
			for (Order order : result) {
				order.setTotalPriceStr(FormatUtils.getMoney(order.getTotalMoney()));
			}
		}
		listOrder.setModel(new ListModelList<Order>(result));
	}
	
	@Listen("onDetail=#listOrder")
	public void showDetaiModalDialog(ForwardEvent evt) {
		Button editBtn = (Button) evt.getOrigin().getTarget();
		
		String editBtnId = editBtn.getId();

		Integer oderId = Integer.valueOf(editBtnId.substring(6));
		
		final HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("orderId", oderId);
		Window window = (Window) Executions.createComponents("detailOrderAdmin.zul", null, map);
		window.doModal();
	}
	
	@Listen("onClick = #clearButton")
	public void reset() {
		keywordBox.setValue("");
	}
}
