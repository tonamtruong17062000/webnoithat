package com.thai.controller.product;

import java.util.HashMap;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.thai.bean.Product;
import com.thai.bean.User;
import com.thai.service.ProductService;
import com.thai.service.impl.ProductServiceImpl;
import com.thai.utils.FormatUtils;

public class AdminController extends SelectorComposer<Component> {

	@Wire
	private Listbox listProduct;

	@Wire
	private Button searchButton;

	@Wire
	private Textbox keywordBox;
	
	@Wire
	private Button clearButton;
	
	
	
	private ProductService productService = new ProductServiceImpl();

	@Override
	public void doAfterCompose(Component component) throws Exception {
		// TODO Auto-generated method stub
		User userSessions = (User) Sessions.getCurrent().getAttribute("userSession");
		if (userSessions == null) {
			Executions.sendRedirect("login.zul");
		} else {
			super.doAfterCompose(component);
			List<Product> products = productService.findBySearch("");
			if (products.size()>0) {
				for (Product product : products) {
					product.setTotalPriceStr(FormatUtils.getMoney(product.getPrice()));
				}
			}
			System.out.println("sakdjak");
			listProduct.setModel(new ListModelList<Product>(products));
		}

	}

//	@Override
//	public void doInit(Page arg0, Map<String, Object> arg1) throws Exception {
//		// TODO Auto-generated method stub
//		System.out.println("sakdjak");
//	}
	@Listen("onClick = #addButton")
	public void showAddModal() {
		Window window = (Window) Executions.createComponents("addProduct.zul", null, null);
		window.doModal();
	}
	
	

	@Listen("onEdit=#listProduct")
	public void showUpdateModalDialog(ForwardEvent evt) {
		// Lấy về đối tượng Edit Button từ ForwardEvent
		Button editBtn = (Button) evt.getOrigin().getTarget();
		// Lấy về id của nút Edit
		String editBtnId = editBtn.getId();
		// Tách ra carId từ id của nút Edit
		Integer productId = Integer.valueOf(editBtnId.substring(4));
		// đưa dữ liệu vào HashMap để truyền vào hộp thoại editCar.zul được mở ra
		final HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("productId", productId);
		Window window = (Window) Executions.createComponents("editProduct.zul", null, map);
		window.doModal();
	}

	@Listen("onDelete=#listProduct")
	public void doDelete(final ForwardEvent evt) {
		Messagebox.show("Question is pressed. Are you sure?", "Question", Messagebox.YES | Messagebox.NO,
				Messagebox.QUESTION, new EventListener<Event>() {
					@Override
					public void onEvent(final Event confirmEvt) throws Exception {
						// TODO Auto-generated method stub
						if (Messagebox.ON_YES.equals(confirmEvt.getName())) {
							// Tách ra carId từ id của nút Delete
							Button deleteBtn = (Button) evt.getOrigin().getTarget();
							String deleteBtnId = deleteBtn.getId();
							Long productIdDel = Long
									.parseLong(String.valueOf(Integer.valueOf(deleteBtnId.substring(6))));
							// Dòng code minh họa lấy được id
							if (productService.deleteById(productIdDel)) {
								Messagebox.show("Xóa sản phẩm thành công !");
							} else {
								Messagebox.show("Xóa sản phẩm thất bại !");
							}
							Executions.sendRedirect("admin.zul");
						} else { // Nếu ko đồng ý xóa
							return;
						}
					}
				});
	}

	@Listen("onClick = #searchButton")
	public void search() {
		String keyword = keywordBox.getValue();
		List<Product> result = productService.findBySearch(keyword);
		if (result.size()>0) {
			for (Product product : result) {
				product.setTotalPriceStr(FormatUtils.getMoney(product.getPrice()));
			}
		}
		listProduct.setModel(new ListModelList<Product>(result));
	}
	@Listen("onClick = #clearButton")
	public void reset() {
		keywordBox.setValue("");
	}
	

}
