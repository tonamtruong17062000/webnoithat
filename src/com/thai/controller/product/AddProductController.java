package com.thai.controller.product;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Blob;
import java.util.List;

import javax.naming.PartialResultException;

import org.zkoss.image.AImage;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Image;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.thai.bean.Product;
import com.thai.service.ProductService;
import com.thai.service.impl.ProductServiceImpl;

//import javafx.animation.ParallelTransition;

public class AddProductController extends SelectorComposer<Component> {

	@Wire
	private Window modalDialog;

	@Wire
	private Image previewImage;

	@Wire
	private Textbox codeProduct;

	@Wire
	private Textbox nameProduct;

	@Wire
	private Textbox priceProduct;

	@Wire
	private Textbox description;

	private ProductService productService = new ProductServiceImpl();

	@Listen("onClick = #addCarBtn")
	public void addProduct() {

		if (previewImage.getContent() == null) {
			Messagebox.show("Vui lòng chọn ảnh", "", Messagebox.OK, Messagebox.ERROR);
			return;
		}
		// Thêm ảnh vào thư mục img
		uploadFile();
		// Gọi phương thức của lớp tầng model để insert các trường dữ liệu vào database
		String code = codeProduct.getValue().toString();
		String name = nameProduct.getValue().toString();
		Double price = Double.parseDouble(priceProduct.getValue().toString());
		String descriptionTxt = description.getValue().toString();
		String imgUrl = previewImage.getContent().getName();
		Product product = new Product(code, name, price, descriptionTxt, "/img/" + imgUrl);

		if (productService.save(product)) {
			Messagebox.show("Add new car successfull!");
		} else {
			Messagebox.show("Error");
		}

		// Đóng hộp thoại
		modalDialog.detach();
		// Quay lại trang quản lý ô tô
		Executions.sendRedirect("admin.zul");
	}

	public void uploadFile() {
		byte[] byteImg = previewImage.getContent().getByteData();
		FileOutputStream fos = null;
		String fileUploadPath = Executions.getCurrent().getDesktop().getWebApp().getRealPath("/img/");
		try {
			fos = new FileOutputStream(fileUploadPath + previewImage.getContent().getName());
			fos.write(byteImg);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				fos.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}
}
