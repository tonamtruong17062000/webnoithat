package com.thai.controller.product;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.thai.bean.Product;
import com.thai.service.ProductService;
import com.thai.service.impl.ProductServiceImpl;

public class EditProductController extends SelectorComposer<Component> {

	@Wire
	private Window modalDialog;

	@Wire
	private Image previewImage;

	@Wire
	private Textbox codeProduct;

	@Wire
	private Textbox nameProduct;

	@Wire
	private Textbox priceProduct;

	@Wire
	private Textbox description;

	@Wire
	private Label msg;
	
	private ProductService productService = new ProductServiceImpl();

	private Product product = new Product();
	
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);

		// Lấy carId được truyền tới bởi trang SearchController qua HashMap
		Integer productId = (Integer) Executions.getCurrent().getArg().get("productId");
		
		Long editProductId = Long.parseLong(String.valueOf(productId));
		// Hiển thị message lên hộp thoại có tính chất minh
		
		
		product = productService.findById(editProductId);
		// Gọi phương thức tầng model load dữ liệu từ database căn cứ vào carId có được
		// set vào các trường thuộc tính để hiển thị lên giao diện trang editCar.zul cho
		// phép sửa
		// ??
		
		codeProduct.setValue(product.getCode());
		nameProduct.setValue(product.getName());
		priceProduct.setValue(String.valueOf(product.getPrice()));
		description.setValue(product.getDescription());
	}

	@Listen("onClick = #updateProductBtn")
	public void update() {
		// Thêm ảnh vào thư mục img

		// Gọi phương thức của lớp tầng model để insert các trường dữ liệu vào database
		String code = codeProduct.getValue().toString();
		String name = nameProduct.getValue().toString();
		Double price = Double.parseDouble(priceProduct.getValue().toString());
		String descriptionTxt = description.getValue().toString();
		product.setCode(code);
		product.setName(name);
		product.setPrice(price);
		product.setDescription(descriptionTxt);
		if (productService.save(product)) {
			Messagebox.show("Update new car successfull!");
		} else {
			Messagebox.show("Error");
		}

		// Đóng hộp thoại
		modalDialog.detach();
		// Quay lại trang quản lý ô tô
		Executions.sendRedirect("admin.zul");
	}

}
