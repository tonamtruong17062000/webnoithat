package com.thai.controller.cart;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.A;
import org.zkoss.zul.Button;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import com.thai.bean.Cart;
import com.thai.bean.Product;
import com.thai.service.ProductService;
import com.thai.service.impl.ProductServiceImpl;
import com.thai.utils.FormatUtils;

public class CartController extends SelectorComposer<Component>{
	private static final long serialVersionUID = 1L;
	@Wire
	private Label titleLabel;

	@Wire
	private Listbox listCart;
	
	@Wire
	private Label totalMoneyLabel;

	@Wire
	private Button btnOrder;
	

	@Wire
	private Menuitem menuAdmin;
	

	private ProductService productService = new ProductServiceImpl();
	Double totalMoney=0.0;
	List<Product> productInCart = new ArrayList<Product>();
	@SuppressWarnings("unchecked")
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		// Gọi lại phương thức lớp cha để khởi tạo các thành phần mặc định
		super.doAfterCompose(comp);
		Double total = (Double) Sessions.getCurrent().getAttribute("totalPrice");
		List<Cart> carts = new ArrayList<Cart>();
 		if (Sessions.getCurrent().getAttribute("listCart") != null) {
			carts= (List<Cart>)Sessions.getCurrent().getAttribute("listCart");
			for (Cart cart : carts) {
				cart.setTotalPriceStr(FormatUtils.getMoney(cart.getTotalPrice()));
				productInCart.add(cart.getProduct());
			}
		}else {
			btnOrder.setDisabled(true);
		}
		
		titleLabel.setValue("Danh sách giỏ hàng");
		if (carts.size()>0) {
			totalMoneyLabel.setValue("Tổng tiền : "+getTotalMoney(carts)+"VNĐ");
			totalMoney = getTotalMoney(carts);
		}
		

		
		listCart.setModel(new ListModelList<Cart>(carts));
	}
	
	public static double getTotalMoney(List<Cart> cartDto) {
		double totalMoney = 0;
		for (Cart itemCart : cartDto) {
			totalMoney += itemCart.getTotalPrice();
		}
		return totalMoney;
	}
	@SuppressWarnings("unchecked")
	@Listen("onDelete=#listCart")
	public void doDelete(final ForwardEvent evt) {
		Messagebox.show("Question is pressed. Are you sure?", "Question", Messagebox.YES | Messagebox.NO,
				Messagebox.QUESTION, new EventListener<Event>() {
					
					@Override
					public void onEvent(final Event confirmEvt) throws Exception {
						// TODO Auto-generated method stub
						if (Messagebox.ON_YES.equals(confirmEvt.getName())) {
							// Tách ra carId từ id của nút Delete
							Button deleteBtn = (Button) evt.getOrigin().getTarget();
							String deleteBtnId = deleteBtn.getId();
							Long cartIdDel = Long
									.parseLong(String.valueOf(Integer.valueOf(deleteBtnId.substring(6))));
							Map<Long,Cart> myCart = (Map<Long, Cart>) Sessions.getCurrent().getAttribute("myCart");
							List<Cart> carts = (List<Cart>) Sessions.getCurrent().getAttribute("listCart");
							for (int i = 0; i < carts.size(); i++) {
								if (cartIdDel.equals(carts.get(i).getProduct().getId())) {
									carts.remove(i);
								}
							}
							myCart.remove(cartIdDel);
							
							//Set lại để ko bị lặp
							Sessions.getCurrent().setAttribute("listCart", carts);
							Messagebox.show("Xóa thành công!");
							Executions.sendRedirect("cart.zul");
						} else { 
							return;
						}
					}
			});
	}
	
	@Listen("onClick = #btnOrder")
	public void showAddModal() {
		final HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("totalMoney", totalMoney);
		map.put("listProductInCart", productInCart);
		Window window = (Window) Executions.createComponents("order.zul", null, map);
		window.doModal();
	}
}
