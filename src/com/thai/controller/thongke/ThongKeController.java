package com.thai.controller.thongke;

import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Chart;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Window;

import com.thai.bean.Product;
import com.thai.bean.User;
import com.thai.service.ProductService;
import com.thai.service.impl.ProductServiceImpl;
import com.thai.utils.FormatUtils;
	
public class ThongKeController extends SelectorComposer<Component>{
	
	private ProductService productService = new ProductServiceImpl();
	
	@Wire
	private Label numProductPay;
	@Override
	public void doAfterCompose(Component component) throws Exception {
		// TODO Auto-generated method stub
		User userSessions = (User) Sessions.getCurrent().getAttribute("userSession");
		if (userSessions == null) {
			Executions.sendRedirect("login.zul");
		} else {
			super.doAfterCompose(component);
			Integer productInOrder = productService.countSumProducOrder();
			numProductPay.setValue(productInOrder.toString());
			
			
		}
	}
}
