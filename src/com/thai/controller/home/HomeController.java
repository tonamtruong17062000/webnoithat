package com.thai.controller.home;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.A;
import org.zkoss.zul.Button;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.ext.Selectable;

import com.thai.bean.Cart;
import com.thai.bean.Product;
import com.thai.bean.User;
import com.thai.service.ProductService;
import com.thai.service.impl.ProductServiceImpl;
import com.thai.utils.FormatUtils;

public class HomeController extends SelectorComposer<Component> {

	private static final long serialVersionUID = 1L;
	@Wire
	private Label titleLabel;

	@Wire
	private Listbox listProduct1;
	@Wire
	private Hlayout productDetail;

	@Wire
	private Listbox listCar;

	@Wire
	private Label totalMoneyLabel;

	@Wire
	private Hbox cartDetail;

	@Wire
	private Label nameLabel;

	@Wire
	private Label priceLabel;
	@Wire
	private Label descriptionLabel;
	@Wire
	private Image previewImage;
	@Wire
	private A back;
	
	@Wire
	private Textbox keyword;
	@Wire
	private Menuitem menuAdmin;

	@Wire
	private Menuitem btnCart;

	private ProductService productService = new ProductServiceImpl();

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		// Gọi lại phương thức lớp cha để khởi tạo các thành phần mặc định
		super.doAfterCompose(comp);
		String keySeach = (String) Executions.getCurrent().getParameter("type");
		if (keyword != null) {
			keyword.setValue(keySeach);
		}
		List<Product> result = productService.findBySearch(keySeach);
		for (Product product : result) {
			product.setTotalPriceStr(FormatUtils.getMoney(product.getPrice()));
		}
		Sessions.getCurrent().setAttribute("productList", result);
		titleLabel.setValue("Danh sách sản phẩm");
		listProduct1.setModel(new ListModelList<Product>(result));

	}

	@SuppressWarnings("unchecked")
	@Listen("onSelect = #listProduct1")
	public void showDetail() {
		// Lấy về các dòng được chọn trên listbox
		Set<Product> selection = ((Selectable<Product>) listProduct1.getModel()).getSelection();
		if (selection != null && !selection.isEmpty()) {
			// Lấy thông tin ô tô đầu tiên trong ds chọn để hiển thị chi tiết
			Product selected = selection.iterator().next();
			// gán giá trị cho các thành phần trong vùng car detail
//			previewImage.setSrc(selected.getPreview());
//			modelLabel.setValue(selected.getModel());
//			makeLabel.setValue(selected.getMake());
			nameLabel.setValue("Tên sản phẩm : " + selected.getName());
			priceLabel.setValue("Giá của sản phẩm : " + String.valueOf(selected.getPrice()));
			descriptionLabel.setValue("Mô tả sản phẩm : " + selected.getDescription());
			previewImage.setSrc(selected.getImgaeUrl());
		}
		titleLabel.setValue("Chi tiết sản phẩm");
		// ẩn khối danh sách ô tô
		listProduct1.setVisible(false);
		// hiện khối thông tin chi tiết về ô tô được chọn
		productDetail.setVisible(true);
	}

	@Listen("onClick = #back")
	public void backCarList() {
		// Hiển thị khối danh sách ô tô
		listProduct1.setVisible(true);
		titleLabel.setValue("Danh sách sản phẩm");
		// ẩn khối thông tin chi tiết ô tô
		productDetail.setVisible(false);
	}

	@Listen("onClick = #menuAdmin")
	public void redirectAdmin() {
		User userSessions = (User) Sessions.getCurrent().getAttribute("user");
		if (userSessions == null) {
			Executions.sendRedirect("login.zul");
		} else {
			Executions.sendRedirect("admin.zul");
		}
	}

	@SuppressWarnings({ "unused", "unchecked" })
	@Listen("onAdd=#listProduct1")
	public void addToCart(ForwardEvent evt) {
		Button editBtn = (Button) evt.getOrigin().getTarget();
		// Lấy về id của nút Edit
		String editBtnId = editBtn.getId();
		Long id = Long.parseLong(editBtnId.substring(3));
		List<Product> productList = (List<Product>) Sessions.getCurrent().getAttribute("productList");
		Map<Long, Cart> cartItem = (Map<Long, Cart>) Sessions.getCurrent().getAttribute("myCart");
		if (cartItem == null) {
			cartItem = new HashMap<Long, Cart>();
			Sessions.getCurrent().setAttribute("myCartNum", cartItem.size());
		}
		Product product = productService.findById(id);
		if (product != null) {
			if (cartItem.containsKey(id)) {
				Cart cart = cartItem.get(id);
				cart.setProduct(product);
				cart.setQuantity(cart.getQuantity() + 1);
				cart.setTotalPrice(product.getPrice() * cart.getQuantity());
				cartItem.put(id, cart);
			} else {
				Cart cart = new Cart();
				cart.setProduct(product);
				cart.setQuantity(1);
				cart.setTotalPrice(product.getPrice() * cart.getQuantity());
				cartItem.put(id, cart);
			}

		}

		List<Cart> listProductInCart = new ArrayList<Cart>();
		if (cartItem != null) {
			for (Map.Entry<Long, Cart> item : cartItem.entrySet()) {
				item.getValue().getProduct().setNum(item.getValue().getQuantity());
				listProductInCart.add(item.getValue());
			}
		}
		Sessions.getCurrent().setAttribute("myCart", cartItem);
		Sessions.getCurrent().setAttribute("listCart", listProductInCart);
		Messagebox.show("Đã thêm vào giỏ hàng");
		Executions.sendRedirect("home.zul");
	}

	

	@Listen("onClick = #btnCart")
	public void detailCart() {

		cartDetail.setVisible(true);
	}
}
