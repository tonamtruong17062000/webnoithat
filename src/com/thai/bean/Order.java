package com.thai.bean;

import java.util.List;

import com.thai.utils.FormatUtils;

public class Order {
	private Long id;
	private String code;
	private String customerName;
	private String customerAddress;
	private String customerPhone;
	private Double totalMoney;
	private String totalPriceStr;
	private List<Product> products;
	public Order(Long id, String code, String customerName, String customerAddress, String customerPhone,
			Double totalMoney) {
		super();
		this.id = id;
		this.code = code;
		this.customerName = customerName;
		this.customerAddress = customerAddress;
		this.customerPhone = customerPhone;
		this.totalMoney = totalMoney;
	}
	
	
	public Order(String code, String customerName, String customerAddress, String customerPhone, Double totalMoney) {
		super();
		this.code = code;
		this.customerName = customerName;
		this.customerAddress = customerAddress;
		this.customerPhone = customerPhone;
		this.totalMoney = totalMoney;
	}


	public Order() {
		super();
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerAddress() {
		return customerAddress;
	}
	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
	public String getCustomerPhone() {
		return customerPhone;
	}
	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}
	public Double getTotalMoney() {
		return totalMoney;
	}
	public void setTotalMoney(Double totalMoney) {
		this.totalMoney = totalMoney;
	}


	public List<Product> getProducts() {
		return products;
	}


	public void setProducts(List<Product> products) {
		this.products = products;
	}


	public String getTotalPriceStr() {
		return totalPriceStr;
	}


	public void setTotalPriceStr(String totalPriceStr) {
		this.totalPriceStr = totalPriceStr;
	}
	
	
}
