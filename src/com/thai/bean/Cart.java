package com.thai.bean;

import com.thai.utils.FormatUtils;

public class Cart {
	private Product product;
	private int quantity;
	private double totalPrice;
	private String totalPriceStr;
	
	public Cart() {
		super();
		
	}
	
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getTotalPriceStr() {
		return totalPriceStr;
	}


	public void setTotalPriceStr(String totalPriceStr) {
		this.totalPriceStr = totalPriceStr;
	}
	
	
	
	
}
