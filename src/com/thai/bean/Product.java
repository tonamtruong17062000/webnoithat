package com.thai.bean;

import com.thai.utils.FormatUtils;

public class Product {
//	id bigint auto_increment primary key not null,
//    code varchar(10),
//    name nvarchar(50),
//    price double,
//    description TEXT,
//    imgae_url nvarchar(250)

	private Long id;
	private String code;
	private String name;
	private double price;
	private String description;
	private String imgaeUrl;
	private int num;
	private Double tolTalPrice;
	private String totalPriceStr;
	public Product(Long id, String code, String name, double price, String description, String imgaeUrl) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		this.price = price;
		this.description = description;
		this.imgaeUrl = imgaeUrl;
	}

	public Product() {

	}

	public Product(String code, String name, double price, String description, String imgaeUrl) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		this.price = price;
		this.description = description;
		this.imgaeUrl = imgaeUrl;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImgaeUrl() {
		return imgaeUrl;
	}

	public void setImgaeUrl(String imgaeUrl) {
		this.imgaeUrl = imgaeUrl;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public Double getTolTalPrice() {
		return tolTalPrice;
	}

	public void setTolTalPrice(Double tolTalPrice) {
		this.tolTalPrice = tolTalPrice;
	}
	
	public String getTotalPriceStr() {
		return totalPriceStr;
	}


	public void setTotalPriceStr(String totalPriceStr) {
		this.totalPriceStr = totalPriceStr;
	}
	
}
