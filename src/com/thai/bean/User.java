package com.thai.bean;

public class User {
	private Long id;
	private String userName;
	private String passWord;
	private String code;
	private String img;
	private String fullName;
	
	
	
	public User() {
		super();
	}

	public User(String userName, String passWord) {
		super();
		this.userName = userName;
		this.passWord = passWord;
	}
	
	public User(Long id, String userName, String code, String img, String fullName) {
		super();
		this.id = id;
		this.userName = userName;
		this.code = code;
		this.img = img;
		this.fullName = fullName;
	}

	

	public User(Long id, String userName, String passWord, String code, String img, String fullName) {
		super();
		this.id = id;
		this.userName = userName;
		this.passWord = passWord;
		this.code = code;
		this.img = img;
		this.fullName = fullName;
	}
	
	public User(String userName, String passWord, String code, String img, String fullName) {
		super();
		this.userName = userName;
		this.passWord = passWord;
		this.code = code;
		this.img = img;
		this.fullName = fullName;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassWord() {
		return passWord;
	}
	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}
	
	
}
