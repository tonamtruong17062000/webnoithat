package com.thai.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.thai.bean.Product;
import com.thai.bean.User;
import com.thai.datasoucre.ConnectDB;

public class UserDAO {

	public UserDAO() {

	}

	public List<User> findBySearch(User user) {
		List<User> listUser = new ArrayList<User>();
		Connection connection = ConnectDB.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {

			String sql = "SELECT * FROM user WHERE user_name LIKE ? AND password LIKE ?";

			pst = connection.prepareStatement(sql);
			pst.setString(1, "%" + user.getUserName() + "%");
			pst.setString(2, "%" + user.getPassWord() + "%");

			rs = pst.executeQuery();
			while (rs.next()) {
//				id bigint auto_increment primary key not null,
//			    code varchar(10),
//			    name nvarchar(50),
//			    price double,
//			    description TEXT,
//			    imgae_url nvarchar(250)

				String userName = rs.getString("user_name");
				String passWord = rs.getString("password");
				User user2 = new User(userName, passWord);
				listUser.add(user2);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				connection.close();
				pst.close();
				rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return listUser;
	}

	public List<User> findBySearch(String keySearch) {
		List<User> userList = new ArrayList<User>();
		Connection connection = ConnectDB.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {

			String sql = "SELECT * FROM user";

			if ((keySearch != null && !"".equals(keySearch))) {
				sql += " WHERE " + "code LIKE ?" + " OR user_name LIKE ? OR fullname LIKE ?";
			}
			pst = connection.prepareStatement(sql);
			if ((keySearch != null && !"".equals(keySearch))) {
				pst.setString(1, "%" + keySearch.toLowerCase() + "%");
				pst.setString(2, "%" + keySearch.toLowerCase() + "%");
				pst.setString(3, "%" + keySearch.toLowerCase() + "%");
			}
			rs = pst.executeQuery();
			while (rs.next()) {
				Long id = rs.getLong("id");
				String code = rs.getString("code");
				String userName = rs.getString("user_name");
				String img = rs.getString("img");
				String fullName = rs.getString("fullname");
				String password = rs.getString("password");
				User user = null;
				if (img != null) {
					user = new User(id, userName, password, code, img, fullName);
				} else {
					user = new User(id, userName, password, code, "", fullName);
				}

				userList.add(user);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				connection.close();
				pst.close();
				rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return userList;
	}
	
	public boolean save(User user) {
		Connection connection = ConnectDB.getConnection();
		PreparedStatement pst = null;
		StringBuilder sql = new StringBuilder("");
		
		if (user.getId() != null) {
			sql.append("UPDATE webdidong.user SET code=?,user_name=?,password=?,img=?,fullname=? WHERE id = ? ");
		}else {
			sql.append("INSERT INTO webdidong.user(code,user_name,password,img,fullname) VALUES(?,?,?,?,?)");
		}
		
		int i=0;
		try {
			pst = connection.prepareStatement(sql.toString());
			pst.setString(1,user.getCode());
			pst.setString(2,user.getUserName());
			pst.setString(3,user.getPassWord());
			pst.setString(4, user.getImg());
			pst.setString(5, user.getFullName());
			if (user.getId()!=null) {
				pst.setLong(6, user.getId());
			}
			i = pst.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				connection.close();
				pst.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if (i==1) {
			return true;
		}
		return false;
	}

	public User findById(Long id) {
		User user = new User();
		Connection connection = ConnectDB.getConnection();
		PreparedStatement pst=null;
		ResultSet rs= null;
		try {
			

			String sql = "SELECT * FROM user WHERE id = ?";

				
			pst = connection.prepareStatement(sql);
			pst.setLong(1, id);
			rs = pst.executeQuery();
			while (rs.next()) {
				Long idUser = rs.getLong("id");
				String code = rs.getString("code");
				String userName = rs.getString("user_name");
				String img = rs.getString("img");
				String fullName = rs.getString("fullname");
				String password = rs.getString("password");
				user = new User(idUser, userName, password, code, img, fullName);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				connection.close();
				pst.close();
				rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return user;
	}

	public boolean deleteById(Long userIdDel) {
		Connection connection = ConnectDB.getConnection();
		PreparedStatement pst = null;
		StringBuilder sql = new StringBuilder("");
		sql.append("DELETE FROM user WHERE id = ?");	
		int i=0;
//		List<Long> idProductExistInProductOrder = new ArrayList<Long>();
		try {
//			for (int j = 0; j < array.length; j++) {
//				
//			}
			pst = connection.prepareStatement(sql.toString());
			pst.setLong(1, userIdDel);			
			i = pst.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				connection.close();
				pst.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if (i==1) {
			return true;
		}
		return false;
	}
	
}
