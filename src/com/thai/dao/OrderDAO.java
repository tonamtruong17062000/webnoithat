package com.thai.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.thai.bean.Order;
import com.thai.bean.Product;
import com.thai.datasoucre.ConnectDB;

public class OrderDAO {

	public boolean save(Order order) {
		Connection connection = ConnectDB.getConnection();
		PreparedStatement pst = null;
		ResultSet resultSet = null;
		StringBuilder sql = new StringBuilder("");

		sql.append(
				"INSERT INTO orders(code,customer_name,customer_address,customer_phone,total_money) VALUES(?,?,?,?,?)");
		String sqInsertProductOrder = "INSERT INTO product_order(product_id,order_id,num) VALUES(?,?,?)";

		int i = 0;
		try {
			pst = connection.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			pst.setString(1, order.getCode());
			pst.setString(2, order.getCustomerName());
			pst.setString(3, order.getCustomerAddress());
			pst.setString(4, order.getCustomerPhone());
			pst.setDouble(5, order.getTotalMoney());
			i = pst.executeUpdate();

			if (i == 1) {
				resultSet = pst.getGeneratedKeys();
				if (resultSet.next()) {
					order.setId(resultSet.getLong(1));
				}
			}

			for (Product product : order.getProducts()) {
				pst = connection.prepareStatement(sqInsertProductOrder);
				pst.setLong(1, product.getId());
				pst.setLong(2, order.getId());
				pst.setLong(3, product.getNum());
				pst.execute();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
				pst.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (i == 1) {
			return true;
		}
		return false;
	}

	public List<Order> findBySearch(String keyWord) {
		List<Order> listOrders = new ArrayList<Order>();
		Connection connection = ConnectDB.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {

			String sql = "SELECT * FROM orders";

			if ((keyWord != null && !"".equals(keyWord))) {
				sql += " WHERE "  
						+" customer_name LIKE ? " 
						+" OR " + "code LIKE ?" 
						+" OR customer_address LIKE ? "
						+" OR customer_phone LIKE ?";
			}
			pst = connection.prepareStatement(sql);
			if ((keyWord != null && !"".equals(keyWord))) {
				pst.setString(1, "%" + keyWord.toLowerCase() + "%");
				pst.setString(2, "%" + keyWord.toLowerCase() + "%");
				pst.setString(3, "%" + keyWord.toLowerCase() + "%");
				pst.setString(4, "%" + keyWord.toLowerCase() + "%");
			}
			rs = pst.executeQuery();
			while (rs.next()) {
				Long id = rs.getLong("id");
				String code = rs.getString("code");
				String customerName = rs.getString("customer_name");
				String customerAddress = rs.getString("customer_address");
				String customerPhone = rs.getString("customer_phone");
				Double totalMoney = rs.getDouble("total_money");
				Order order = new Order(id, code, customerName, customerAddress, customerPhone, totalMoney);
				listOrders.add(order);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				connection.close();
				pst.close();
				rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return listOrders;
	}

}
