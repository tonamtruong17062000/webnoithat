package com.thai.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.thai.bean.Product;
import com.thai.datasoucre.ConnectDB;

public class ProductDAO {

	public ProductDAO() {

	}

	public List<Product> findBySearch(String keySearch) {
		List<Product> listProduct = new ArrayList<Product>();
		Connection connection = ConnectDB.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {

			String sql = "SELECT * FROM product";

			if ((keySearch != null && !"".equals(keySearch))) {
				sql += " WHERE " + "name LIKE ? " + " OR " + "code LIKE ?" + " OR price LIKE ?";
			}
			pst = connection.prepareStatement(sql);
			if ((keySearch != null && !"".equals(keySearch))) {
				pst.setString(1, "%" + keySearch.toLowerCase() + "%");
				pst.setString(2, "%" + keySearch.toLowerCase() + "%");
				pst.setString(3, "%" + keySearch.toLowerCase() + "%");
			}
			rs = pst.executeQuery();
			while (rs.next()) {
				Long id = rs.getLong("id");
				String code = rs.getString("code");
				String name = rs.getString("name");
				Double price = rs.getDouble("price");
				String description = rs.getString("description");
				String imgaeUrl = rs.getString("imgae_url");
				Product product = null;
				if (imgaeUrl != null) {
					product = new Product(id, code, name, price, description, imgaeUrl);
				} else {
					product = new Product(id, code, name, price, description, "");
				}

				listProduct.add(product);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				connection.close();
				pst.close();
				rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return listProduct;
	}

	public boolean save(Product product) {
		Connection connection = ConnectDB.getConnection();
		PreparedStatement pst = null;
		StringBuilder sql = new StringBuilder("");

		if (product.getId() != null) {
			sql.append("UPDATE webdidong.product SET code=?,name=?,price=?,description=?,imgae_url=? WHERE id = ? ");
		} else {
			sql.append("INSERT INTO webdidong.product(code,name,price,description,imgae_url) VALUES(?,?,?,?,?)");
		}

		int i = 0;
		try {
			pst = connection.prepareStatement(sql.toString());
			pst.setString(1, product.getCode());
			pst.setString(2, product.getName());
			pst.setDouble(3, product.getPrice());
			pst.setString(4, product.getDescription());
			pst.setString(5, product.getImgaeUrl());
			if (product.getId() != null) {
				pst.setLong(6, product.getId());
			}
			i = pst.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
				pst.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (i == 1) {
			return true;
		}
		return false;
	}

	public Product findById(Long id) {
		Product product = new Product();
		Connection connection = ConnectDB.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {

			String sql = "SELECT * FROM product WHERE id = ?";

			pst = connection.prepareStatement(sql);
			pst.setLong(1, id);
			rs = pst.executeQuery();
			while (rs.next()) {
				Long idPr = rs.getLong("id");
				String code = rs.getString("code");
				String name = rs.getString("name");
				Double price = rs.getDouble("price");
				String description = rs.getString("description");
				String imgaeUrl = rs.getString("imgae_url");
				product = new Product(idPr, code, name, price, description, imgaeUrl);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				connection.close();
				pst.close();
				rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return product;
	}

	@SuppressWarnings("resource")
	public boolean deleteById(Long productIdDel) {
		Connection connection = ConnectDB.getConnection();
		PreparedStatement pst = null;
		StringBuilder sql = new StringBuilder("");
		sql.append("DELETE FROM product WHERE id = ?");
		String sqlDelete = "DELETE FROM product_order WHERE product_id = ?";
		int i = 0;
		try {
			pst = connection.prepareStatement(sqlDelete);
			pst.setLong(1, productIdDel);
			pst.executeUpdate();

			pst = connection.prepareStatement(sql.toString());
			pst.setLong(1, productIdDel);
			i = pst.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
				pst.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (i == 1) {
			return true;
		}
		return false;
	}

	public List<Product> findByOrderId(Long orderId) {
		List<Product> listProduct = new ArrayList<Product>();
		Connection connection = ConnectDB.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {

			String sql = "SELECT pr.name," + " pr.price," + " pr.imgae_url," + " od.customer_name," + " po.num,"
					+ " (pr.price*po.num) as totalPrice" + " FROM product pr inner join product_order po "
					+ " on pr.id = po.product_id " + " inner join orders od "
					+ " on po.order_id = od.id WHERE od.id = ?";

			pst = connection.prepareStatement(sql);
			pst.setLong(1, orderId);
			rs = pst.executeQuery();
			while (rs.next()) {
				String name = rs.getString("name");
				Double price = rs.getDouble("price");
				String imgaeUrl = rs.getString("imgae_url");
				int num = rs.getInt("num");
				Double totalPrice = rs.getDouble("totalPrice");
				Product product = new Product();
				product.setName(name);
				product.setPrice(price);
				product.setImgaeUrl(imgaeUrl);
				product.setNum(num);
				product.setTolTalPrice(totalPrice);
				listProduct.add(product);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				connection.close();
				pst.close();
				rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return listProduct;
	}

	public int countSumProducOrder() {
		int count = 0;
		Connection connection = ConnectDB.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT count(num) as number_product FROM webdidong.product_order";
			pst = connection.prepareStatement(sql);
			rs = pst.executeQuery();
			while (rs.next()) {
				count = rs.getInt("number_product");
			}
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				connection.close();
				pst.close();
				rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return count;
	}
}
