package com.thai.service;

import java.util.List;

import com.thai.bean.Product;

public interface ProductService {

	List<Product> findBySearch(String keySearch);
	
	Product findById(Long id);
	boolean save(Product product);

	boolean deleteById(Long productIdDel);

	List<Product> findByOrderId(Long orderId);

	int countSumProducOrder();

}
