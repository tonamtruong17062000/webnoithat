package com.thai.service.impl;

import java.util.List;

import com.thai.bean.User;
import com.thai.dao.UserDAO;
import com.thai.service.UserService;

public class UserServiceImpl implements UserService {

	private UserDAO userDAO;

	public UserServiceImpl() {
		// TODO Auto-generated constructor stub
		userDAO = new UserDAO();
	}

	@Override
	public boolean checkLogin(User user) {
		boolean check = false;
		if (userDAO.findBySearch(user).size() == 1) {
			check = true;
		}
		return check;
	}

	@Override
	public List<User> findBySearch(String keySearch) {
		// TODO Auto-generated method stub
		return userDAO.findBySearch(keySearch);
	}

	@Override
	public boolean save(User user) {
		// TODO Auto-generated method stub
		boolean check = userDAO.save(user);
		if (check) {
			return true;
		}
		return false;
	}

	@Override
	public User findById(Long editUserId) {
		return userDAO.findById(editUserId);
	}

	@Override
	public boolean deleteById(Long userIdDel) {
		// TODO Auto-generated method stub
		boolean checkDelStatus = userDAO.deleteById(userIdDel);
		return checkDelStatus;
	}

}
