package com.thai.service.impl;

import java.sql.Connection;
import java.util.List;

import com.thai.bean.Order;
import com.thai.dao.OrderDAO;
import com.thai.datasoucre.ConnectDB;
import com.thai.service.OrderService;

public class OrderServiceImpl implements OrderService{
	private OrderDAO orderDAO;
	public OrderServiceImpl() {
		orderDAO = new OrderDAO();
	}
	@Override
	public boolean save(Order order) {
		
		return orderDAO.save(order);
	}
	@Override
	public List<Order> findBySearch(String keyWord) {
		// TODO Auto-generated method stub
		return orderDAO.findBySearch(keyWord);
	}

}
