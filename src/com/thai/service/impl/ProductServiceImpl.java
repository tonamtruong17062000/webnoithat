package com.thai.service.impl;

import java.util.List;

import com.thai.bean.Product;
import com.thai.dao.ProductDAO;
import com.thai.service.ProductService;

public class ProductServiceImpl implements ProductService{
	private ProductDAO productDAO;
	
	public ProductServiceImpl() {
		// TODO Auto-generated constructor stub
		productDAO = new ProductDAO();
	}
	@Override
	public List<Product> findBySearch(String keySearch) {
		return productDAO.findBySearch(keySearch);
	}
	@Override
	public boolean save(Product product) {
		// TODO Auto-generated method stub
		boolean check = productDAO.save(product);
		if (check) {
			return true;
		}
		return false;
	}
	@Override
	public Product findById(Long id) {
		return productDAO.findById(id);
	}
	@Override
	public boolean deleteById(Long productIdDel) {
		// TODO Auto-generated method stub
		boolean checkDelStatus = productDAO.deleteById(productIdDel);
		return checkDelStatus;
	}
	@Override
	public List<Product> findByOrderId(Long orderId) {
		// TODO Auto-generated method stub
		return productDAO.findByOrderId(orderId);
	}
	@Override
	public int countSumProducOrder() {
		// TODO Auto-generated method stub
		return productDAO.countSumProducOrder();
	}

}
