package com.thai.service;

import java.util.List;

import com.thai.bean.User;

public interface UserService {
	public boolean checkLogin(User user);

	public List<User> findBySearch(String string);

	public boolean save(User user);

	public User findById(Long editUserId);

	public boolean deleteById(Long userIdDel); 
}
