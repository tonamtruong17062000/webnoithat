package com.thai.service;

import java.util.List;

import com.thai.bean.Order;

public interface OrderService {

	boolean save(Order order);

	List<Order> findBySearch(String string);

}
